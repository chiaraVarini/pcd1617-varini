package pcd.ass01.ex1;

import pcd.ass01.ex1.solution.optimized.MandelbrotSetImageConcurrImplOptimizedImpl;

/**
 * Simple Mandelbrot Set Viewer 
 *		 
 * @author aricci
 *
 */
public class MandelbrotViewer {
	public static void main(String[] args) throws Exception {
		
		/* size of the mandelbrot set in pixel */
		int width = 4000;
		int height = 4000;
		
		/* number of iteration */
		int nIter = 500;

		/* region to be represented: center and radius */
		Complex c0 = new Complex(0,0);
		double rad0 = 2;


		/*Complex c0 = new Complex(-0.75,0.1);
		double rad0 = 0.02;
		
		Complex c0 = new Complex(0.7485,0.0505);
		double rad0 = 0.000002;

		Complex c0 = new Complex(0.254,0);
		double rad0 = 0.001;*/
	

		
		/* creating the set */
		//MandelbrotSetImage set = new MandelbrotSetImageImpl(width,height, c0, rad0);
		//MandelbrotSetImage set = new MandelbrotSetImageConcurImpl(width,height, c0, rad0);
		MandelbrotSetImage set = new MandelbrotSetImageConcurrImplOptimizedImpl(width,height, c0, rad0);

		System.out.println("Computing...");
		StopWatch cron = new StopWatch();
		cron.start();
		
		/* computing the image */
		set.compute(nIter);
		cron.stop();
		System.out.println("done - "+cron.getTime()+" ms");

		/* showing the image */
		MandelbrotView view = new MandelbrotView(set,1000,800);
		view.setVisible(true);

	}

}
