package pcd.ass01.ex1.solution.base;

import pcd.ass01.ex1.Complex;

/**
 * Created by chiaravarini on 26/03/17.
 */
public class MandelbrotSetImageConcurImpl extends BaseMandelbrotSetImage {

    private final Worker[] workers;

    public MandelbrotSetImageConcurImpl(int width, int height, Complex center, double rad0) {
        super(width, height,  center, rad0);
        this.workers = new Worker[Runtime.getRuntime().availableProcessors()+1];
    }

    /**
     * Compute parallel
     * @param nIterMax number of iteration representing the level of detail
     */
    @Override
    public void compute(int nIterMax) {

        int nRows = getWidth()/ workers.length;
        int initialRow = 0;

        for(int i = 0; i<workers.length-1; i++){

            workers[i] = new Worker(initialRow, nRows, getHeight(), getImage(), nIterMax, getWidth(), this);
            workers[i].start();
            initialRow += nRows;
        }

        workers[workers.length - 1] = new Worker(initialRow,getWidth()-initialRow, getHeight(), getImage(), nIterMax, getWidth(), this);
        workers[workers.length - 1].start();

        try {
            for (Worker w: workers){
                w.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
