package pcd.ass01.ex1.solution.base;

import pcd.ass01.ex1.Complex;
import pcd.ass01.ex1.MandelbrotSetImage;

/**
 * Created by chiaravarini on 26/03/17.
 */
public class Worker extends Thread{

    private final int initialRow, w, h, nIterMax, totWidth;
    private final int[] image;
    private final MandelbrotSetImage setImage;


    public Worker(final int initialRow, final int w, final int h, final int[] image,
                  final int nIterMax, final int totWidth, final MandelbrotSetImage setImage){
        this.initialRow = initialRow;
        this.totWidth = totWidth;
        this.w = w;
        this.h = h;
        this.image = image;
        this.nIterMax = nIterMax;
        this.setImage = setImage;
    }


    @Override
    public void run() {
        for (int x = initialRow; x < initialRow+w; x++ ){
            for (int y = 0; y < h; y++){
                Complex c = setImage.getPoint(x,y);
                double level = MandelbrotAlgorithm.computeColor(c,nIterMax);
                int color = (int)(level*255);
                image[y*totWidth+x] = color + (color << 8)+ (color << 16);
            }
        }
    }
}
