package pcd.ass01.ex1.solution.base;

import pcd.ass01.ex1.Complex;
import pcd.ass01.ex1.MandelbrotSetImage;

/**
 * Created by chiaravarini on 26/03/17.
 */
public abstract class BaseMandelbrotSetImage implements MandelbrotSetImage{


    private final static double MULTIPLICATIVE_CONSTANT_RADIUS = 0.5;
    private final int width, height;
    private volatile int image[];
    private final Complex  center;
    private final double delta;


    public BaseMandelbrotSetImage(final int width,final int height,final Complex center, final double rad0){
        this.width = width;
        this.height = height;
        this.image = new int[width*height];
        this.center = center;
        this.delta = rad0 / (width * MULTIPLICATIVE_CONSTANT_RADIUS);
    }

    @Override
    public Complex getPoint(int x, int y) {
        return new Complex((x - width*0.5)*delta + center.re(), center.im() - (y - height*0.5)*delta);
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int[] getImage() {
        return image;
    }

}
