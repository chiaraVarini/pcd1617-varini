package pcd.ass01.ex1.solution.base;

import pcd.ass01.ex1.Complex;

/**
 * Created by chiaravarini on 26/03/17.
 */
public final class MandelbrotAlgorithm {

    public static double computeColor(final Complex c, final int maxIteration){
            int iteration = 0;
            Complex z = new Complex(0,0);

		/*
		 * Repeatedly compute z := z^2 + c
		 * until either the point is out
		 * of the 2-radius circle or the number
		 * of iteration achieved the max value
		 *
		 */
            while ( z.absFast() <= 2 &&  iteration < maxIteration ){
                z = z.times(z).plus(c);
                iteration++;
            }
            if ( iteration == maxIteration ){
			  /* the point belongs to the set */
                return 0;
            } else {
			  /* the point does not belong to the set => distance */
                return 1.0-((double)iteration)/maxIteration;
            }
        }
}
