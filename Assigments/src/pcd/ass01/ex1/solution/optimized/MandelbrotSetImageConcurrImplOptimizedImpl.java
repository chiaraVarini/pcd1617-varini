package pcd.ass01.ex1.solution.optimized;

import pcd.ass01.ex1.Complex;
import pcd.ass01.ex1.solution.base.BaseMandelbrotSetImage;

/**
 * Created by chiaravarini on 28/03/17.
 */
public class MandelbrotSetImageConcurrImplOptimizedImpl extends BaseMandelbrotSetImage implements MandelbrotSetImageConcurrOptimized {

    private int countPiece = 0;
    private final int imageDivisor;
    private final WorkerEfficently[] workersEff;

    public MandelbrotSetImageConcurrImplOptimizedImpl(int width, int height, Complex center, double rad0) {
        super(width, height, center, rad0);
        this.workersEff = new WorkerEfficently[Runtime.getRuntime().availableProcessors()+1];
        this.imageDivisor = width / (width/150);
    }

    @Override
    public void compute(int nIterMax){
        int nRows = getWidth()/ imageDivisor;

        for(int i = 0; i<workersEff.length; i++){
            workersEff[i] = new WorkerEfficently(nRows, getHeight(), getImage(), nIterMax, getWidth(), this);
            workersEff[i].start();
        }
        try {
            for (WorkerEfficently w: workersEff){
                w.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized int getNextStartPiece() {
        if( countPiece < getWidth()) {
            int res = countPiece;
            countPiece += getWidth() / imageDivisor;
            return res;
        }else {
            return -1;
        }
    }
}
