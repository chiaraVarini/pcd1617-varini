package pcd.ass01.ex1.solution.optimized;

import pcd.ass01.ex1.Complex;
import pcd.ass01.ex1.solution.base.MandelbrotAlgorithm;

/**
 * Created by chiaravarini on 27/03/17.
 */
public class WorkerEfficently extends Thread{


    private int initialRow, w, h, nIterMax, totWidth;
    private final int[] image;
    private final MandelbrotSetImageConcurrOptimized setImage;


    public WorkerEfficently( final int w, final int h, final int[] image,
                             final int nIterMax, final int totWidth, final MandelbrotSetImageConcurrOptimized setImage){
        this.initialRow = 0;
        this.totWidth = totWidth;
        this.w = w;
        this.h = h;
        this.image = image;
        this.nIterMax = nIterMax;
        this.setImage = setImage;
    }


    @Override
    public void run() {
        while (initialRow >= 0) {
            initialRow = setImage.getNextStartPiece();
            for (int x = initialRow; x < initialRow + w && initialRow!=-1; x++) {
                for (int y = 0; y < h; y++) {
                    Complex c = setImage.getPoint(x, y);
                    double level = MandelbrotAlgorithm.computeColor(c, nIterMax);
                    int color = (int) (level * 255);
                    if(y*totWidth+x < image.length) {
                        image[y * totWidth + x] = color + (color << 8) + (color << 16);
                    }
                }
            }
        }
    }
}
