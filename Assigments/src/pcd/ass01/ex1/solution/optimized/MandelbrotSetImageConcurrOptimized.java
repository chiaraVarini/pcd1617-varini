package pcd.ass01.ex1.solution.optimized;

import pcd.ass01.ex1.MandelbrotSetImage;

/**
 * Created by chiaravarini on 28/03/17.
 */
public interface MandelbrotSetImageConcurrOptimized extends MandelbrotSetImage {

    int getNextStartPiece();
}
