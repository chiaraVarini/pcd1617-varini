package pcd.ass01.ex2;

/**
 * Created by chiaravarini on 29/03/17.
 */

public class Counter{

    private int value;

    public Counter(int initialValue){this.value = initialValue;}
    public Counter(){this(0);}

    public void inc(){
        value++;
    }

    public int getValue(){
        return value;
    }

}
