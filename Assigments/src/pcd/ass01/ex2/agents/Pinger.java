package pcd.ass01.ex2.agents;

import pcd.ass01.ex2.Counter;

import java.util.concurrent.Semaphore;

/**
 * Created by chiaravarini on 29/03/17.
 */
public class Pinger extends Thread{

    private Counter counter;
    private Semaphore ping;
    private Semaphore changeValue;
    private Semaphore stampedValue;

    private boolean stop = false;

    public Pinger(Counter c, Semaphore ping, Semaphore changeValue, Semaphore stampedValue){
        counter = c;
        this.ping = ping;
        this.changeValue = changeValue;
        this.stampedValue = stampedValue;
    }

    public void run(){

        try{
            while(!stop) {

                stampedValue.acquire();
                counter.inc();
                changeValue.release();

                System.out.println("Ping!");

                ping.release();

                Thread.sleep(500);
            }
        }  catch (Exception e) {
            e.printStackTrace();
        } finally {
            changeValue.release();
            System.out.println("Goodbye Ping");
        }
    }

    public void stopThread(){
        this.stop= true;
    }

}
