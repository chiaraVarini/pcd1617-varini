package pcd.ass01.ex2.agents;


import pcd.ass01.ex2.Counter;

import java.util.concurrent.Semaphore;

/**
 * Created by chiaravarini on 29/03/17.
 */
public class Ponger extends Thread{

    private Counter counter;
    private Semaphore ping;
    private Semaphore changeValue;
    private Semaphore stampedValue;
    private boolean stop = false;

    public Ponger(Counter c,  Semaphore ping, Semaphore changeValue, Semaphore stampedValue){
        counter = c;
        this.ping = ping;
        this.changeValue = changeValue;
        this.stampedValue = stampedValue;
    }

    public void run(){

        try {
            ping.acquire();

            while(!stop) {

                stampedValue.acquire();
                counter.inc();
                changeValue.release();

                System.out.println("Pong!");
                Thread.sleep(500);

            }
        } catch (InterruptedException e) {
            e.printStackTrace();

        } finally {
            changeValue.release();
            System.out.println("Goodbye Pong");
        }
    }

    public void stopThread(){
        this.stop = true;
    }

}
