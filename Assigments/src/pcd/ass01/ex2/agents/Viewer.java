package pcd.ass01.ex2.agents;

import pcd.ass01.ex2.Counter;

import java.util.concurrent.Semaphore;

/**
 * Created by chiaravarini on 29/03/17.
 */
public class Viewer extends Thread {

    private Counter counter;
    private Semaphore changeValue;
    private Semaphore stampedValue;
    private boolean stop = false;

    public Viewer(Counter counter, Semaphore changeValue, Semaphore stampedValue){
        this.counter=counter;
        this.changeValue = changeValue;
        this.stampedValue = stampedValue;
    }

    @Override
    public void run() {

        try {
            while(!stop) {

                changeValue.acquire();
                System.out.println(counter.getValue());
                stampedValue.release();

            }
        }catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Goodbye Viewer");
    }

    public void stopThread(){
        this.stop = true;
    }
}
