package pcd.ass01.ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Created by chiaravarini on 29/03/17.
 */
public class Gui extends JFrame implements ActionListener {

    private Controller controller;

    public Gui(Controller controller){
        super("Ping-Pong Concurrent");
        setSize(300,100);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        this.controller = controller;

        JButton start = new JButton("START");
        start.addActionListener(this);
        JButton stop = new JButton("STOP");
        stop.addActionListener(this);

        getContentPane().setLayout(new FlowLayout());
        getContentPane().add(start);
        getContentPane().add(stop);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        controller.processEvent(e.getActionCommand());
    }
}
