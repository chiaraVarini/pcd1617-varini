package pcd.ass01.ex2;

/**
 * Created by chiaravarini on 29/03/17.
 */
public class Main {

    public static void main(String[] args) {
        Counter counter = new Counter();
        Controller controller = new Controller(counter);
        new Gui(controller);
    }
}
