package pcd.ass01.ex2;


import pcd.ass01.ex2.agents.Pinger;
import pcd.ass01.ex2.agents.Ponger;
import pcd.ass01.ex2.agents.Viewer;

import java.util.concurrent.Semaphore;

/**
 * Created by chiaravarini on 29/03/17.
 */
public class Controller{

    private Pinger pinger;
    private Ponger ponger;
    private Viewer viewer;

    private Semaphore ping = new Semaphore(0);
    private Semaphore stampedValue = new Semaphore(0);
    private Semaphore changeValue = new Semaphore(1);


    public Controller(Counter counter){

        this.pinger = new Pinger(counter, ping, changeValue, stampedValue);
        this.ponger = new Ponger(counter, ping, changeValue, stampedValue);
        this.viewer = new Viewer(counter, changeValue, stampedValue);
    }

    public void processEvent(String actionCommand){

        if(actionCommand.equals("START")){
            ponger.start();
            pinger.start();
            viewer.start();

        } else if(actionCommand.equals("STOP")){
            pinger.stopThread();
            ponger.stopThread();
            viewer.stopThread();

            try {
                pinger.join();
                ponger.join();
                viewer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.exit(0);
        }
    }
}
