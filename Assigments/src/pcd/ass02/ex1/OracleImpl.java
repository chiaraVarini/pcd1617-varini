package pcd.ass02.ex1;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by chiaravarini on 18/04/17.
 */
public class OracleImpl implements Oracle {

    private static OracleImpl SINGLETON;

    private final long magicNumber;
    private boolean gameFinished;
    private int playerNumber;
    private int turn = 0;
    private int counter = 0;


    public static OracleImpl getIstance(final int playerNumber){
        if(SINGLETON==null) {
            synchronized (OracleImpl.class) {
                SINGLETON = new OracleImpl(ThreadLocalRandom.current().nextLong(0, Long.MAX_VALUE), playerNumber);
            }
        }
        return SINGLETON;
    }

    private OracleImpl(final long magicNumber,final int playerNumber){
        this.magicNumber = magicNumber;
        this.gameFinished = false;
        this.playerNumber = playerNumber;
        System.out.println("Generated Orcale with: " + magicNumber + " value.");
    }

    @Override
    public synchronized boolean isGameFinished() {
        return this.gameFinished;
    }

    @Override
    public synchronized Result tryToGuess(int playerId, long value) throws GameFinishedException {

        Result res = new ResultImpl(magicNumber, value);

        if(res.found()){
            this.gameFinished = true;
            throw new GameFinishedException();
        }

        counter ++;

        if(counter % playerNumber == 0){
            turn++;
        }
        return res;
    }

    @Override
    public synchronized int getTurn(){
        return turn;
    }
}
