package pcd.ass02.ex1;

import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by chiaravarini on 18/04/17.
 */
public class Player extends Thread{

    private final int id;
    private final Oracle oracle;
    private int turn = 0;
    private boolean winner = false;

    private long lowerBound = 0;
    private long upperBound = Long.MAX_VALUE;
    private final Set<Long> triedValues = new TreeSet<Long>((x,y)->(int)(x-y));


    public Player(final int id, final Oracle oracle){
        this.id = id;
        this.oracle = oracle;
    }

    @Override
    public void run() {

        long tryValue = ThreadLocalRandom.current().nextLong(lowerBound, Long.MAX_VALUE);

        while(!oracle.isGameFinished()){

            if(oracle.getTurn()==turn) {
                try {

                    System.out.println(id + " Player try: " + tryValue + " in bound: " + lowerBound + " to " + upperBound);
                    Result res = oracle.tryToGuess(id, tryValue);
                    triedValues.add(tryValue);

                    if (res.isGreater() && upperBound > tryValue) {
                        this.upperBound = tryValue;

                    } else if (res.isLess() && lowerBound < tryValue) {
                        this.lowerBound = tryValue;
                    }

                    do {

                        tryValue = ThreadLocalRandom.current().nextLong(lowerBound, upperBound);

                    } while (triedValues.contains(tryValue));

                    turn++;

                } catch (GameFinishedException ex) {
                    winner = true;
                }
            }
        }
        if(!winner){
            System.out.println("Sob.. " + id + " Player");
        } else {
            System.out.println("Win!! " +id + " Player");
        }

    }
}
