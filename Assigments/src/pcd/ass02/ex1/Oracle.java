package pcd.ass02.ex1;

public interface Oracle {

	boolean isGameFinished();

	Result tryToGuess(int playerId, long value) throws GameFinishedException;

	int getTurn();
	
}
