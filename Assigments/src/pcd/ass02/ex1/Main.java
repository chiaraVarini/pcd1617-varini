package pcd.ass02.ex1;

/**
 * Created by chiaravarini on 18/04/17.
 */
public class Main {

    private final static int NUMBER_OF_PLAYER = 10;

    public static void main(String[] args) {

        Oracle oracle = OracleImpl.getIstance(NUMBER_OF_PLAYER);

        for(int i = 0; i < NUMBER_OF_PLAYER; i++){
            new Player(i, oracle).start();
        }
    }
}
