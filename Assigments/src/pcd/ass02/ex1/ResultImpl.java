package pcd.ass02.ex1;

/**
 * Created by chiaravarini on 18/04/17.
 */
public class ResultImpl implements Result {

    private final long oracleValue;
    private final long playerValue;

    public ResultImpl(long oracleValue, long playerValue){
        this.oracleValue = oracleValue;
        this.playerValue = playerValue;
    }


    @Override
    public boolean found() {
        return oracleValue == playerValue;
    }

    @Override
    public boolean isGreater() {
        return  playerValue > oracleValue;
    }

    @Override
    public boolean isLess() {
        return  playerValue < oracleValue;
    }
}
