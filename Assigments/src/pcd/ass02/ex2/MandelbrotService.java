package pcd.ass02.ex2;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by chiaravarini on 19/04/17.
 */
public class MandelbrotService extends Thread {

    private ExecutorService executor;
    private MandelbrotSetImageImplOpt set;
    private int poolSize;

    public MandelbrotService(int poolSize,   MandelbrotSetImageImplOpt set){
        this.executor = Executors.newFixedThreadPool(poolSize);
        this.set = set;
        this.poolSize = poolSize;
    }

    public synchronized void compute(int nIterMax){


        Set<Future<Void>> resultSet = new HashSet<>();
        int w = set.getWidth();
        int h = set.getHeight();
        double delta = set.getDelta();
        Complex center= set.getCenter();

        //per ogni x istanzio un task
        for (int x = 0; x < w; x++ ){
            Future<Void> res = executor.submit(new MandelbrotTask(set, nIterMax, x, h, w, delta, center));
            resultSet.add(res);
        }

        for (Future<Void> future : resultSet) {
            try {
                future.get();
            } catch (Exception ex){
                ex.printStackTrace();
            }
        }

    }

    public void shutdownService(){
        executor.shutdown();
    }

    private void log(String msg){
        System.out.println("[SERVICE] "+msg);
    }


}
