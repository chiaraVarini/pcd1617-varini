package pcd.ass02.ex2;


/**
 * Simple Mandelbrot Set Viewer 
 *		 
 * @author aricci
 *
 */
public class MandelbrotSequentialAnimator1 {
	public static void main(String[] args) throws Exception {
		
		/* size of the mandelbrot set in pixel */
		int width = 600;
		int height = 600;
		
		/* number of iteration */
		int nIter = 500;

		/* region to be represented: center and radius */
		Complex c0 = new Complex(-0.75,0);
		double rad0 = 2;

		Complex c1 = new Complex(-0.75,0.1);
		Complex c2 = new Complex(-0.1011,0.9563);
		Complex c3 = new Complex(0.254,0);
		Complex c4 = new Complex(0.001643721971153, 0.822467633298876);
		
		/* creating the set */
		MandelbrotSetImageImplOpt set = new MandelbrotSetImageImplOpt(width,height, c4, rad0);

		System.out.println("Computing w:"+width+"|h:"+height+"|nIt:"+nIter+"...");
		StopWatch cron = new StopWatch();

		/* showing the image */
		MandelbrotView view = new MandelbrotView(set,600,600);
		view.setVisible(true);

		int poolSize = Runtime.getRuntime().availableProcessors()+1;
		MandelbrotService service = new MandelbrotService(poolSize, set);

		double radius = rad0;

		/* simple animation */

		while (true) {
			if (view.isRunImage()) {
				cron.start();
				service.compute(nIter);
				cron.stop();
				System.out.println("Frame computed in " + cron.getTime() + " ms");
				radius *= 0.9;
				set.updateRadius(radius);
				view.repaint();
			} else if(view.stopPressed()) {
                service.shutdownService();
            }
		}
	}

}
