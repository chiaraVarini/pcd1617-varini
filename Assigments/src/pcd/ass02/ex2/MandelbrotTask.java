package pcd.ass02.ex2;

import java.util.concurrent.Callable;


/**
 * Computa una colonna intera di pixel
 * Created by chiaravarini on 19/04/17.
 */
public class MandelbrotTask implements Callable<Void> {

    private int nIterMax, x, h, w;
    private double delta;
    private Complex center;
    private MandelbrotSetImageImplOpt image;

    public MandelbrotTask(MandelbrotSetImageImplOpt image, int nIterMax, int x, int h, int w, double delta, Complex center){
        this.image = image;
        this.nIterMax = nIterMax;
        this.x = x;
        this.h = h;
        this.w = w;
        this.delta = delta;
        this.center = center;
    }

    public Void call() {

        for (int y = 0; y < h; y++) {
            double x0 = (x - w * 0.5) * delta + center.re();
            double y0 = center.im() - (y - h * 0.5) * delta;
            double level = image.computeColor(x0, y0, nIterMax);
            int color = (int) (level * 255);
            image.getImage()[y * w + x] = color + (color << 8) + (color << 16);
        }
        return null;
    }

    protected void log(String msg){
        System.out.println("["+Thread.currentThread().getName()+"] "+msg);
    }
}
