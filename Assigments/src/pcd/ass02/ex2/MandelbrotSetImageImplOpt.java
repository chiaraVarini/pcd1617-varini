package pcd.ass02.ex2;

/**
 * Class for computing an image of a region
 * of the Mandelbrot set.
 * 
 * @author aricci
 *
 */
public class MandelbrotSetImageImplOpt implements MandelbrotSetImage {
	
	private int w,h;
	private int image[]; 
	private Complex center;
	private double delta;
	
		
	/**
	 * Creating an empty Mandelbrot set image. 
	 * 
	 * @param w width in pixels
	 * @param h height in pixels
	 * @param c center of the Mandelbrot set to be represented
	 * @param radius radius of the Mandelbrot set to be represented
	 */
	public MandelbrotSetImageImplOpt(int w, int h, Complex c, double radius){
		this.w = w;
		this.h = h;
        image = new int[w*h];
        center = c;
        delta = radius/(w*0.5);
	}
	
	/**
	 * 
	 * Compute the image with the specified level of detail
	 * 
	 * See https://en.wikipedia.org/wiki/Mandelbrot_set
	 * 
	 * @param nIterMax number of iteration representing the level of detail
	 */
	public void compute(int nIterMax){	
		
		/*
		 * for each pixel of the image
		 * - get the corresponding point on the complex plan
		 * - verify if the point either belongs or not to the Mandelbrot set
		 * -- yes => black color 
		 * -- no => level of gray, depending on the distance from the set
		 */

		for (int x = 0; x < w; x++ ){
			for (int y = 0; y < h; y++){
				double x0 = (x - w*0.5)*delta + center.re();
				double y0 = center.im() - (y - h*0.5)*delta;
				double level = computeColor(x0,y0,nIterMax);
				int color = (int)(level*255);
				image[y*w+x] = color + (color << 8)+ (color << 16);
			}
		}
	}


	public void compute(int x, int y, int nIterMax){
		double x0 = (x - w*0.5)*delta + center.re();
		double y0 = center.im() - (y - h*0.5)*delta;
		double level = computeColor(x0,y0,nIterMax);
		int color = (int)(level*255);
		image[y*w+x] = color + (color << 8)+ (color << 16);
	}




    /**
	 * Basic Mandelbrot set algorithm optimized
	 *
	 * @param maxIteration
	 * @return
	 */
	public double computeColor(double x0, double y0, int maxIteration){
		int iteration = 0;		
		double x = 0;
		double y = 0;
		double x2 = x*x;
		double y2 = y*y;
		while (x2 + y2 < 4  &&  iteration < maxIteration) {
			double xtemp = x2 - y2 + x0;
			y = 2*x*y + y0;
			x = xtemp;
			x2 = x*x;
			y2 = y*y;
			iteration++;
		}
				  
		if ( iteration == maxIteration ){			  
			  /* the point belongs to the set */
			  return 0;
		  } else {
			  /* the point does not belong to the set => distance */
			  return 1.0-((double)iteration)/maxIteration;
		  }
	}
	
	/**
	 * This method returns the point in the complex plane
	 * corresponding to the specified element/pixel in the image
	 * 
	 * @param x x coordinate in the image
	 * @param y y coordinate in the image
	 * @return the corresponding complex point
	 */
	public Complex getPoint(int x, int y){
		return new Complex((x - w*0.5)*delta + center.re(), center.im() - (y - h*0.5)*delta); 
	}
	
	/**
	 * Get the height of the image
	 * @return
	 */
	public int getHeight(){
		return h;
	}

	/**
	 * Get the width of the image
	 * @return
	 */
	public int getWidth(){
		return w;
	}
	
	/**
	 * Get the image as an array of int, organized per rows
	 * (compatible with the BufferedImage style)
	 * 
	 * @return
	 */
	public int[] getImage(){
		return image;
	}
	
	
	public void updateRadius(double rad){
        delta = rad/(w*0.5);		
	}

	public double getDelta(){return  this.delta;}

	public Complex getCenter(){return  this.center;}


}

