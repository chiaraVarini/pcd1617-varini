package pcd.ass03;

import javax.swing.*;
import java.awt.*;

/**
 * Created by chiaravarini on 09/05/17.
 */
public class Gui extends JFrame {

    private JLabel secretNumber = new JLabel("secretNumber: ");
    private JLabel turn = new JLabel("turn: 0");
    private JLabel winnerID = new JLabel("");

    public Gui(){
        super();
        this.setLocationRelativeTo(null);
        this.setSize(new Dimension(500,100));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JButton start = new JButton("Strat");
        JButton stop = new JButton("Stop");

        JPanel panel = new JPanel();
        panel.add(start);
        panel.add(stop);
        panel.add(secretNumber);
        panel.add(turn);
        panel.add(winnerID);
        this.setContentPane(panel);
        this.setVisible(true);
    }

    public void setSecretNumber(String secretNumber) {
        this.secretNumber.setText("secretNumber: " + secretNumber);
    }

    public void setTurn(String turn) {
        this.turn.setText("turn: " +turn);
    }

    public void setWinnerID(String winnerID) {
        this.winnerID.setText("the winner is: " +winnerID+ " player!");
    }
}
